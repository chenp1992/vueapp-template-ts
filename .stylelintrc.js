module.exports = {
    extends: '@startdt/stylelint-config',
    rules: {
        'max-line-length': 160,
        'font-family-name-quotes': null,
        'font-family-no-missing-generic-family-keyword': null,
        'plugin/no-unsupported-browser-features': null,
        'font-weight-notation': null,
        'no-duplicate-selectors': null,
        'selector-pseudo-element-no-unknown': null,
        'declaration-block-no-duplicate-properties': null,
        'function-calc-no-invalid': null,
        'property-no-unknown':null,
        'stylus/property-no-unknown':null
    },
};
