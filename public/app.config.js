/* eslint-disable no-undef */
// 应用全局配置对象
window.AppConfig = {

  // 系统标题
  systemTitle: '系统标题',

  // api代理前缀
  apiPrefix: '/api',

};
