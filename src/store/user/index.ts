/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Module, VuexModule, MutationAction, Action, getModule } from 'vuex-module-decorators';
import { removeToken, getToken, setToken, getUserInfo, setUserInfo } from '@/utils/storage';
import { RouteConfig } from 'vue-router';
import { asyncRouteMap } from '@/router/route.config';
import store from '@/store';
import { LoginInfo } from './types';
import { omit } from 'lodash-es';

const generateRoutes = () => asyncRouteMap;

@Module({
  dynamic: true,
  namespaced: true,
  name: 'user',
  store,
})
class UserModule extends VuexModule {
    _loginInfo: LoginInfo | null = null

    _asyncRoutes: RouteConfig[] = []

    // 登录用户信息
    get userInfo(): LoginInfo | null {
      return this._loginInfo;
    }

    // 动态加载的路由配置
    get asyncRoutes(): RouteConfig[] {
      return this._asyncRoutes;
    }

    @Action
    setUserInfo(loginInfo: LoginInfo) {
      const userInfo: LoginInfo = omit(loginInfo, ['token']);
      setUserInfo(JSON.stringify(userInfo));
      setToken(loginInfo.token!);
    }

    @MutationAction({ mutate: ['_loginInfo', '_asyncRoutes'] })
    async getUserInfo() {
      if (!getToken()) {
        throw Error('用户token不存在!');
      }
      let userInfo: LoginInfo | null = null;
      const userData = getUserInfo();
      if (userData) {
        try {
          // 从storage中获取用户信息
          userInfo = JSON.parse(userData);
        } catch (error) {
          userInfo = null;
        }
      } else {
        throw Error('用户信息不存在!');
      }
      return {
        _loginInfo: userInfo,
        _asyncRoutes: generateRoutes(),
      };
    }

    @MutationAction({ mutate: ['_loginInfo'] })
    async logout() {
      removeToken();
      setTimeout(() => {
        window.location.reload();
      }, 200);
      return {
        _loginInfo: null,
      };
    }
}

export default getModule(UserModule);
