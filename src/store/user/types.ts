export interface LoginInfo {
    name: string,
    token?: string,
    roles?: string[],
}
