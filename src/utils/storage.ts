import { USER_TOKEN_KEY, USER_INFO_KEY, USER_HISTORY_KEY } from './constant';

// history
export const getHistory = () => JSON.parse((sessionStorage.getItem(USER_HISTORY_KEY) as string));
export const setHistory = (history: any[]) => sessionStorage.setItem(USER_HISTORY_KEY, JSON.stringify(history));
export const removeHistory = () => sessionStorage.removeItem(USER_HISTORY_KEY);

// token
export const getToken = () => localStorage.getItem(USER_TOKEN_KEY);
export const setToken = (token: string) => localStorage.setItem(USER_TOKEN_KEY, token);
export const removeToken = () => localStorage.removeItem(USER_TOKEN_KEY);

// 用户信息
export const getUserInfo = () => localStorage.getItem(USER_INFO_KEY);
export const setUserInfo = (userInfo: string) => localStorage.setItem(USER_INFO_KEY, userInfo);
export const clearUserInfo = () => localStorage.removeItem(USER_INFO_KEY);
