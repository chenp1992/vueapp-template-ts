/* eslint-disable @typescript-eslint/no-explicit-any */

// 应用全局配置对象
export const appConfig = (window as any).AppConfig || {};

export const applyMixins = (derivedCtor: any, baseCtors: any[]) => {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
};

export const setDocumentTitle = () => {
  document.title = appConfig.systemTitle;
};
