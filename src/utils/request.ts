/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import { ResponseData } from '@/service/common/types';
import { getToken, removeToken } from '@/utils/storage';
import { appConfig } from '@/utils/utility';
import { Message } from 'element-ui';

const showMessage = Message;

const instance = axios.create({
  baseURL: appConfig.apiPrefix || '',
  timeout: 10000,
});

const codeMessage: { [index: number]: string } = {
  '4003': '无访问权限',
  '4004': 'token不存在或者已过期',
  '4053': '没有权限访问该资源',
};

// 请求拦截器
instance.interceptors.request.use((config: AxiosRequestConfig) => {
  config.headers.common.token = getToken();
  return config;
});

// 响应拦截器
instance.interceptors.response.use(
  (response: AxiosResponse) => response, (error: AxiosError) => {
    if (axios.isCancel(error)) {
      console.log('请求已取消', error.message);
    } else {
      // error.message && showMessage.error({
      //   message: error.message,
      //   duration: 3000,
      // });
      console.log(error.message);
    }
    return error;
  },
);

// http响应处理
function handleResponse<T>(response: AxiosResponse): T {
  if (navigator.onLine) {
    let errorMsg = '';
    const contentType: string = response.headers['content-type'];
    if (contentType.includes('application/json')) {
      const resdata: ResponseData<T> = response.data as ResponseData<T>;
      if (resdata) {
        if (resdata.success) {
          return resdata.value;
        }
        // 处理请求业务逻辑错误
        errorMsg = resdata.codeNum ? codeMessage[resdata.codeNum] : '';
        if (errorMsg) {
          removeToken();
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        } else {
          errorMsg = resdata.codeDesc || '请求失败';
        }
      } else {
        errorMsg = '响应数据与接收类型不匹配';
      }
    } else {
      const resdata: T = response.data as T;
      if (resdata) {
        return resdata;
      }
      errorMsg = '响应数据与接收类型不匹配';
    }

    // 默认弹出异常信息
    errorMsg && showMessage.error({
      message: errorMsg,
      duration: 3000,
    });

    // 接口业务逻辑错误，直接抛出
    throw Error(errorMsg);
  } else {
    showMessage.error({
      message: '网络异常',
    });
    throw Error('网络异常');
  }
}

export default {
  axios: instance,
  async request<T = any>(
    config: AxiosRequestConfig,
  ): Promise<T> {
    try {
      return handleResponse<T>(await instance.request(config));
    } catch (error) {
      return Promise.reject(error);
    }
  },
  async post<T = any>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<T> {
    try {
      return handleResponse<T>(await instance.post(url, data, config));
    } catch (error) {
      return Promise.reject(error);
    }
  },
  async get<T = any>(
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<T> {
    try {
      return handleResponse<T>(await instance.get(url, config));
    } catch (error) {
      return Promise.reject(error);
    }
  },
  async put<T = any>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<T> {
    try {
      return handleResponse<T>(await instance.put(url, data, config));
    } catch (error) {
      return Promise.reject(error);
    }
  },
  async patch<T = any>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<T> {
    try {
      return handleResponse<T>(await instance.patch(url, data, config));
    } catch (error) {
      return Promise.reject(error);
    }
  },
  async delete<T = any>(
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<T> {
    try {
      return handleResponse<T>(await instance.delete(url, config));
    } catch (error) {
      return Promise.reject(error);
    }
  },
};

export { AxiosResponse, AxiosError };
