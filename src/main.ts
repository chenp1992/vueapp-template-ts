import Vue from 'vue';

import ModuleLoader from '@vue-async/module-loader';

// import Loading from './components/canvasTemplate/registerTemplate';
// import EditorConfigure from '@maleon/config-sider-v2';
import EditorConfigure from '../packages/config-sider-v2/src';
import MaleonComponent from '../packages/maleon-components/install';

import App from './App.vue';
import router from './router';
import store from './store';

import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';

import ElementUI from './plugins/element';
import VModal from 'vue-js-modal';

Vue.use(ModuleLoader);
Vue.use(MaleonComponent);
Vue.use(EditorConfigure);

Vue.use(ElementUI);
Vue.prototype.$ELEMENT = { size: 'small' };

Vue.use(VModal, {
  draggable: true,
  resizable: false,
});

Vue.config.productionTip = false;

dayjs.locale('zh-cn');

// eslint-disable-next-line @typescript-eslint/no-var-requires
const customParseFormat = require('dayjs/plugin/customParseFormat');

dayjs.extend(customParseFormat);

const app: Vue = new Vue({
  router,
  store,
  moduleLoader: new ModuleLoader(),
  render: h => h(App),
});

app.$moduleLoader({
  CurrencyLineTest: 'http://172.16.4.218/CurrencyLineTest-dist/maleon-chart-00011.umd.min.js',
  CurrencyBarTest: 'http://172.16.4.218/CurrencyBarTest-dist/maleon-chart-00011.umd.min.js',
  InputTextCtrl: 'http://172.16.4.218/InputText-dist/maleon-chart-00011.umd.min.js',
}).then(() => {
  app.$mount('#app');
});
