import { VueConstructor } from 'vue';

import {
  Cascader,
  Popover,
  Tabs,
  TabPane,
  CascaderPanel,
  Checkbox,
  Collapse,
  CollapseItem,
  PageHeader,
  Pagination,
  DatePicker,
  Switch,
  Autocomplete,
  Upload,
  Dialog,
  Form,
  FormItem,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Input,
  InputNumber,
  Radio,
  RadioButton,
  RadioGroup,
  Select,
  Option,
  Button,
  Tooltip,
  Icon,
  Row,
  Col,
  Table,
  TableColumn,
  InfiniteScroll,
  MessageBox,
  Message,
  Loading,
} from 'element-ui';


const element = {
  install(Vue: VueConstructor) {
    Vue.prototype.$ELEMENT = { size: 'medium' };
    Vue.use(Tabs);
    Vue.use(TabPane);
    Vue.use(Popover);
    Vue.use(CascaderPanel);
    Vue.use(Cascader);
    Vue.use(Checkbox);
    Vue.use(Collapse);
    Vue.use(CollapseItem);
    Vue.use(PageHeader);
    Vue.use(Loading);
    Vue.use(Pagination);
    Vue.use(DatePicker);
    Vue.use(Switch);
    Vue.use(Autocomplete);
    Vue.use(Dialog);
    Vue.use(Upload);
    Vue.use(Form);
    Vue.use(FormItem);
    Vue.use(Dropdown);
    Vue.use(DropdownMenu);
    Vue.use(DropdownItem);
    Vue.use(Input);
    Vue.use(InputNumber);
    Vue.use(Radio);
    Vue.use(RadioButton);
    Vue.use(RadioGroup);
    Vue.use(Select);
    Vue.use(Option);
    Vue.use(Button);
    Vue.use(Tooltip);
    Vue.use(Icon);
    Vue.use(Row);
    Vue.use(Col);
    Vue.use(Table);
    Vue.use(TableColumn);
    Vue.use(InfiniteScroll);
    Vue.prototype.$confirm = MessageBox.confirm;
    Vue.prototype.$msgbox = MessageBox;
    Vue.prototype.$message = Message;
  },
};
export default element;
