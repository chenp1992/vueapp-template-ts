import Vue from 'vue';
import VueRouter, { RouteConfig, Route, NavigationGuardNext } from 'vue-router';
import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css';
import { getToken } from '@/utils/storage';
import userStore from '@/store/user';
import { setDocumentTitle } from '@/utils/utility';
import { isEmpty, startsWith } from 'lodash-es';


NProgress.configure({ showSpinner: false }); // NProgress Configuration

const loginPath = '/login';
const whiteList = [loginPath, '/register', '/share'];

// 判断跳转路径是否在路由白名单中
const inWhiteList = (path: string): boolean => {
  let index = 0;
  let isWhiteList = startsWith(path, whiteList[index]);
  while (!isWhiteList && index < whiteList.length) {
    // eslint-disable-next-line no-plusplus
    index++;
    isWhiteList = startsWith(path, whiteList[index]);
  }
  return isWhiteList;
};

export default (router: VueRouter) => {
  router.beforeEach(async (to: Route, from: Route, next: NavigationGuardNext<Vue>) => {
    // start progress bar
    NProgress.start();

    setDocumentTitle();

    const userToken = getToken();

    if (inWhiteList(to.path)) {
      // 当进入的页面为白名单页面时直接跳转主页
      next();
      NProgress.done();
      return;
    }

    if (userToken) {
      if (userStore.userInfo) {
        next();
        NProgress.done();
      } else {
        try {
          // 获取登录用户信息
          await userStore.getUserInfo();
          // 动态添加可访问路由表
          if (!isEmpty(userStore.asyncRoutes)) {
            userStore.asyncRoutes.forEach((route: RouteConfig) => {
              router.addRoute(route);
            });
          }
          // 请求带有 redirect 重定向时，登录自动重定向到该地址
          if (from.query.redirect) {
            const redirect = from.query.redirect as string;
            next({ path: decodeURIComponent(redirect), replace: to.path === redirect });
          } else {
            next({ path: to.fullPath });
          }
          NProgress.done();
        } catch (error) {
          userStore.logout();
        }
      }
    } else {
      next({ path: loginPath, query: { redirect: to.fullPath } });
      NProgress.done();
    }
  });

  router.afterEach(() => {
    NProgress.done();
  });
};
