/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import Vue from 'vue';
import VueRouter, { Route, RawLocation } from 'vue-router';
import { constantRouteMap } from './route.config';
import routerHandler from './handler';

// 解决Vue-Router升级导致的Uncaught(in promise) navigation guard问题
const originalPush = VueRouter.prototype.push;
// eslint-disable-next-line vue/max-len
VueRouter.prototype.push = function push(location: RawLocation, onComplete?: Function | undefined, onAbort?: (err: Error) => void): Promise<Route> {
  if (onComplete || onAbort) {
    return originalPush.call<VueRouter, any[], Promise<Route>>(this, location, onComplete, onAbort);
  }
  return originalPush.call<VueRouter, any[], Promise<Route>>(this, location).catch(err => err);
};

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: constantRouteMap,
});

// 注册全局路由拦截器
routerHandler(router);

export default router;
