import { RouteConfig } from 'vue-router';

const _import = (file: string) => () => import(`@/views/${file}.vue`).then(m => m.default);

/**
 * 基础路由
 */
export const constantRouteMap: RouteConfig[] = [
  {
    path: '/login',
    name: 'login',
    component: _import('Login'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('@/components/user/RegisterModular.vue'),
  },
  {
    path: '/share/:shareKey',
    name: 'share',
    component: _import('ShareView'),
    props: true,
  },
];

export const asyncRouteMap: RouteConfig[] = [
  {
    path: '/editor/:screenId',
    name: 'editor',
    component: _import('EditorView'),
    props: true,
  },
  {
    path: '/preview/:screenId',
    name: 'preview',
    component: _import('PreviewView'),
    props: true,
  },
  {
    path: '/',
    name: 'home',
    component: _import('LayoutView'),
    redirect: '/project',
    children: [
      {
        path: '/project',
        name: 'project',
        component: _import('ProjectView'),
      },
      {
        path: '/mydata',
        name: 'mydata',
        component: _import('MydataView'),
      },
    ],
  },
];

