export interface LoginParam {
    account: string,
    password: string
}

export interface LoginResult {
    token: string,
}

