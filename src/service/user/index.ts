import request from '@/utils/request';
import { LoginParam, LoginResult } from './types';

// 注册
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const register = async (param: any) => request.post('/v/user/register', param);

// 登录
export const login = async (param: LoginParam) => request.post<LoginResult>('/v/user/login', param);

// 登出
export const logout = async () => request.post<LoginResult>('/v/user/logout');


