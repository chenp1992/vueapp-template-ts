# Vue+Typescript 踩坑记录

## Vue+Typescript使用注意事项

---

### vue文件引入必须带上.vue扩展名

> 在文件shims-vue.d.ts中，定义了'*.vue'，依靠扩展名来辨别vue文件，所有在引入vue文件时必须带上.vue扩展名
> 这是为了typescript做的适配定义文件，因为.vue 文件不是一个常规的文件类型，ts 是不能理解 vue 文件是干嘛的，
> 加入shims-vue.d.ts是告诉ts，vue 文件是这种类型的。

**正例**

```dotnetcli
import ComponentA from '../components/index.vue'

import ComponentB from '../components/ComponentB.vue'
```

**反例**

```dotnetcli
import ComponentA from '../components/index'

import ComponentB from '../components/ComponentB'

import ComponentC from '../components/ComponentC/index'
```

### 变量声明阶段，不可以使用this

> 准备来说，并非不可使用，而是在此时this的指向并不是组件实例。
> 解决方案，bind，call，apply了解一下

> 常见错误用法在ant design vue的Form组件使用中

**正例**

```dotnetcli
export default class LoginForm {
  form: any = null
  
  created() {
    this.form = this.$form.createForm(this);
    
  }
}
```

**反例**

```dotnetcli
export default class LoginForm {
  form: any = this.$form.createForm(this);
  created() {
    
  }
}
```

### tsx和render函数

> 在使用tsx编写render函数时，h（$createElement）不会被自动注入。
> [参考链接](https://github.com/vuejs/babel-plugin-transform-vue-jsx/issues/174)(Babel插件解决)

**正例**

```dotnetcli
@Component
export default class HelloWorld extends Vue {
  render(h){
    return <div>HelloWorld</div>
  }
}
```

或

```dotnetcli
@Component
export default class HelloWorld extends Vue {
  render(){
    const h = this.$createElement
    return <div>HelloWorld</div>
  }
}
```

**反例**

```dotnetcli
@Component
export default class HelloWorld extends Vue {
  render(){
    return <div>HelloWorld</div>
  }
}
```

## Typescript易混概念

---

### 命名空间不应与模块混用

> 命名空间是位于全局命名空间下的一个普通的带有名字的JavaScript对象。 这令命名空间十分容易使用。 它们可以在多文件中同时使用，并通过 --outFile结合在一起。 命名空间是帮你组织Web应用不错的方式，你可以把所有依赖都放在HTML页面的 &lt;script&gt;标签里。但就像其它的全局命名空间污染一样，它很难去识别组件之间的依赖关系，尤其是在大型的应用中。

参考[命名空间和模块](https://www.tslang.cn/docs/handbook/namespaces-and-modules.html)

## 优秀Typescript文章

---

1. [巧用 Typescript](https://zhuanlan.zhihu.com/p/39620591)
2. [巧用 Typescript (二)](https://zhuanlan.zhihu.com/p/64423022)

## todo

---

1. vue文件的在template 和style 中，路径提示不正确
2. vue文件中函数参数似乎没有校验问题确认
