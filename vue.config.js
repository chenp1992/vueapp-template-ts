const path = require('path');
const webpack = require('webpack');
// eslint-disable-next-line no-unused-vars
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const resolve = (dir) => {
    return path.join(__dirname, dir);
};

// vue.config.js
module.exports = {
    configureWebpack: {
        // webpack plugins
        plugins: [
            // Ignore all locale files of moment.js
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        ],
        // if prod is on, add externals
        // externals: isProd() ? prodExternals : {},
        // 警告 webpack 的性能提示
        performance: {
            hints: 'warning',
            // 入口起点的最大体积
            maxEntrypointSize: 50000000,
            // 生成文件的最大体积
            maxAssetSize: 30000000,
            // 只给出 js 文件的性能提示
            assetFilter: function (assetFilename) {
                return assetFilename.endsWith('.js');
            },
        },
    },
    chainWebpack: config => {
        // 路径别名设置
        config.resolve.alias
            .set('@$', resolve('src'));

        // 当执行analyze命令时启用打包分析
        if (process.env.NODE_ENV === 'production') {
            if (process.env.VUE_APP_ANALYZE) {
                // 使用webpack-bundle-analyzer 生成报表
                config.plugin('analyzer')
                    .use(new BundleAnalyzerPlugin());
            }
        }

        // 第三方库单独打包
        config
            .optimization.merge({
            splitChunks: {
                maxInitialRequests: 8,
                cacheGroups: {
                    ...['axios', 'lodash-es', 'element-ui', 'echarts'].reduce((r, o, i) => {
                        r[o] = {
                            name: o,
                            test: new RegExp(o),
                            chunks: 'all',
                            priority: 1,
                        };
                        return r;
                    }, {}),
                },
            },
        })
            .end();
    },
    css: {
        loaderOptions: {
            less: {
                lessOptions: {
                    javascriptEnabled: true,
                },
            },
        },
    },
    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'less',
            patterns: [
                path.resolve(__dirname, './src/style/base.less'),
            ],
        },
    },
    productionSourceMap: false,
    transpileDependencies: [
        'vuex-module-decorators',
    ],
    devServer: {
        port: 8002,
        proxy: {
            '/api': {
                // target: 'http://test-maleon-data-02.k8s.startdtapi.com', // maleon2.0服务端
                target: 'http://192.168.8.102:8080',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '',
                },
            },
        },
    },
};
