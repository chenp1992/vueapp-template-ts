module.exports = {
    root: true,
    parser: 'vue-eslint-parser',
    extends: '@startdt/eslint-config',
    parserOptions: {
        parser: '@typescript-eslint/parser',
        extraFileExtensions: ['.vue'],
        ecmaVersion: 2020,
        ecmaFeatures: {
            jsx: true,
        },
    },
    plugins: [
        'vue'
    ],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 2 : 1,
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 1,
        // 自定义规则
        'max-len': 0,
        'semi': 2,
        'quotes': 2,
        'max-lines': 0,
        'new-cap': 0,
        'indent': [1, 2],
        'compat/compat': 0,
        'object-curly-newline': 0,
        'no-inner-declarations': 1,
        'no-unused-expressions': 0,
        'no-unused-vars': 0,
        'prefer-destructuring': 0,
        'import/no-extraneous-dependencies': 0,
        'import/no-named-as-default': 0,
        'import/extensions': 0,
        'import/order': 0,
        'node/no-extraneous-import': 0,
        'node/no-unpublished-import': 0,
        'vue/max-len': 0,
        'vue/no-unused-refs': 0,
        'vue/require-name-property': 0,
        'vue/no-boolean-default': 0,
        'vue/no-unused-properties': 1,
        'vue/require-prop-type-constructor': 2,
        'vue/require-direct-export': 0,
        'vue/html-indent': [1, 2, {
            "attribute": 1,
            "baseIndent": 1,
            "closeBracket": 0,
            "alignAttributesVertically": true,
            "ignores": []
        }],
        'vue/script-indent': [1, 2, {
            "baseIndent": 0,
            "switchCase": 0,
            "ignores": []
        }],
        'vue/valid-v-for': 0,
        '@typescript-eslint/explicit-module-boundary-types': 0,
    },
};
